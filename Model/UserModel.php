<?php
namespace App\Model;
use App\Model\DB;
use PDO;

require '../vendor/autoload.php';
class UserModel {
    private $name;
    private $email;
    private $password;
    protected $db;
    //public $conn;
    public function __construct()
    {   
        
        $this->db = new DB();
        
        $this->db->connect();
        
       
        
    }
    public function add($name,$email,$password){
        $stmt = $this->db->con->prepare('INSERT INTO user (name, email, password) values (?, ?, ?)');
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->email);
        $stmt->bindParam(3, $this->password);
        $this->name=$name;
        $this->email=$email;
        $this->password=$password;
        $stmt->execute();
       
     }
    public function login($email,$password){
        $sql='SELECT email, password
        FROM user
        WHERE email = :email AND password = :password';
        $sth = $this->db->con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array('email' => $email, 'password' => $password));
        $row=$sth->fetch();
        var_dump($row);
        return $row;
    }
     
}
